import { Body, Controller, Get, Post } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat-dto';

@Controller('cats')
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Post()
  async create(@Body() createCatDto: CreateCatDto) {
    if (createCatDto) {
    }
    this.catsService.create(createCatDto);
    return `This action add new cat ${createCatDto.name}`;
  }

  @Get()
  findAll(): string {
    return 'This   action returns all cats';
  }
}

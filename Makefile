BASE_DIR = $(shell pwd);

build-image:
	docker build -t my-nest-js .

run-dev:
	docker run --rm -it --user 1000:1000 -p 3000:3000 -w /usr/src/app  -v  $(shell pwd):/usr/src/app  my-nest-js npm run start:dev

create-project:
	docker run --rm -it --user 1000:1000 -p 3000:3000 -w /usr/src/app  -v  $(shell pwd):/usr/src/app  my-nest-js nest new my_project

ls-files:
	echo ${BASE_DIR}

